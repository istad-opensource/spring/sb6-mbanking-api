package co.istad.mbankingapi.exception;

import co.istad.mbankingapi.base.BaseError;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ServiceException {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = ResponseStatusException.class)
    public BaseError<?> handleServiceException(ResponseStatusException e) {
        return BaseError.builder()
                .message("Something went wrong!")
                .code(e.getStatusCode().value())
                .timestamp(LocalDateTime.now())
                .errors(e.getReason())
                .build();
    }
}
