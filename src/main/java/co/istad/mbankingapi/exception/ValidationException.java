package co.istad.mbankingapi.exception;

import co.istad.mbankingapi.base.BaseError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestControllerAdvice
public class ValidationException {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    BaseError<?> handleMethodArgumentNotValid(MethodArgumentNotValidException e) {

        List<Map<String, String>> errors = new ArrayList<>();
        e.getFieldErrors().forEach(fieldError -> {
            Map<String, String> error = new HashMap<>();
            error.put("field", fieldError.getField());
            error.put("reason", fieldError.getDefaultMessage());
            errors.add(error);
        });

        return BaseError.builder()
                .message("Validation is error!")
                .code(4000)
                .timestamp(LocalDateTime.now())
                .errors(errors)
                .build();
    }

}
