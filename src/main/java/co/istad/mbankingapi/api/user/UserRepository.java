package co.istad.mbankingapi.api.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    // How to retrieve data
    // 1. Derived Query Methods
    void deleteByUuid(String uuid);
    boolean existsByUuid(String uuid);
    Optional<User> findByUuid(String uuid);

    Optional<User> findByUuidAndIsDeletedFalse(String uuid);

    // 2. JPQL
    @Modifying
    @Query("UPDATE User AS u SET u = :user WHERE u.uuid = :uuid")
    void updateUserInfoByUuid(String uuid, User user);

    @Modifying
    @Query("UPDATE User AS u SET u.isDeleted = TRUE WHERE u.uuid = ?1")
    void disableUserByUuid(String uuid);


    // 3. Native SQL
    // 4. Named Query

}
