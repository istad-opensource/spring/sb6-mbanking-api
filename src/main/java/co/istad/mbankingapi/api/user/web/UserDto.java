package co.istad.mbankingapi.api.user.web;

import co.istad.mbankingapi.api.authority.web.RoleDto;

import java.util.List;

public record UserDto(String uuid,
                      String name,
                      String gender,
                      String profile,
                      String email,
                      String phoneNumber,
                      Boolean isStudent,
                      String studentCardNo,
                      List<UserRoleDto> userRoles) {
}
