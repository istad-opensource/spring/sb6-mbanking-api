package co.istad.mbankingapi.api.user.web;

import co.istad.mbankingapi.api.user.UserService;
import co.istad.mbankingapi.base.BaseApi;
import co.istad.mbankingapi.base.BasePagingApi;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @DeleteMapping("/{uuid}/disable")
    void disableUserByUuid(@PathVariable String uuid) {
        userService.disableUserByUuid(uuid);
    }

    @PutMapping("/{uuid}")
    void updateUserByUuid(@PathVariable String uuid,
                          @RequestBody @Valid UpdateUserInfoDto updateUserInfoDto) {
        userService.updateUserByUuid(uuid, updateUserInfoDto);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{uuid}")
    void deleteUserByUuid(@PathVariable String uuid) {
        userService.deleteUserByUuid(uuid);
    }

    @GetMapping("/{uuid}")
    BaseApi<?> loadUserByUuid(@PathVariable String uuid) {
        return userService.loadUserByUuid(uuid);
    }

    @GetMapping
    BasePagingApi<?> loadUsers(@RequestParam(required = false, defaultValue = "0") int page,
                               @RequestParam(required = false, defaultValue = "20") int size) {

        return userService.loadUsers(page, size);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    void createNew(@RequestBody @Valid CreateUserDto createUserDto) {
        userService.createNew(createUserDto);
    }

}
