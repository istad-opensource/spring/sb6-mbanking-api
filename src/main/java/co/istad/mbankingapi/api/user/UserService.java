package co.istad.mbankingapi.api.user;

import co.istad.mbankingapi.api.user.web.CreateUserDto;
import co.istad.mbankingapi.api.user.web.UpdateUserInfoDto;
import co.istad.mbankingapi.api.user.web.UserDto;
import co.istad.mbankingapi.base.BaseApi;
import co.istad.mbankingapi.base.BasePagingApi;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {

    void disableUserByUuid(String uuid);

    void updateUserByUuid(String uuid, UpdateUserInfoDto updateUserInfoDto);

    void deleteUserByUuid(String uuid);

    BaseApi<?> loadUserByUuid(String uuid);

    BasePagingApi<?> loadUsers(int page, int size);

    void createNew(CreateUserDto createUserDto);

}
