package co.istad.mbankingapi.api.user.web;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.List;

public record CreateUserDto(@NotBlank(message = "Name is required!")
                            @Size(min = 5, max = 30)
                            String name,
                            @NotBlank(message = "Gender is required!")
                            String gender,
                            @NotBlank(message = "Email is required!")
                            @Email
                            String email,
                            @NotBlank(message = "Password is required!")
                            String password,
                            @NotBlank(message = "Confirm password is required!")
                            String confirmedPassword,
                            @NotBlank(message = "Phone number is required!")
                            String phoneNumber,
                            @NotNull(message = "Is student is required!")
                            Boolean isStudent,
                            String studentCardNo,
                            List<Long> roleIds) {
}
