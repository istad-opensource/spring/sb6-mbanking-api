package co.istad.mbankingapi.api.user;

import co.istad.mbankingapi.api.authority.Role;
import co.istad.mbankingapi.api.user.web.CreateUserDto;
import co.istad.mbankingapi.api.user.web.UpdateUserInfoDto;
import co.istad.mbankingapi.api.user.web.UserDto;
import co.istad.mbankingapi.base.BaseApi;
import co.istad.mbankingapi.base.BasePaging;
import co.istad.mbankingapi.base.BasePagingApi;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final UserMapper userMapper;

    @Transactional
    @Override
    public void disableUserByUuid(String uuid) {

        if (!userRepository.existsByUuid(uuid))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with UUID = %s is not found", uuid));

        userRepository.disableUserByUuid(uuid);
    }

    @Transactional
    @Override
    public void updateUserByUuid(String uuid, UpdateUserInfoDto updateUserInfoDto) {
        // Partial Update
        // 1. Load old user
        User user = userRepository.findByUuid(uuid).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("User with UUID = %s is not found", uuid)));

        // 2. Update new value to entity
        userMapper.mapUpdateUserInfoDtoToUser(updateUserInfoDto, user);

        // 3. Invoke save to update
        userRepository.save(user);


        //userRepository.updateUserInfoByUuid(user.getUuid(), user);
    }

    @Transactional
    @Override
    public void deleteUserByUuid(String uuid) {
        if (!userRepository.existsByUuid(uuid))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with UUID = %s is not found", uuid));

        userRepository.deleteByUuid(uuid);
    }

    @Override
    public BaseApi<?> loadUserByUuid(String uuid) {

        User user = userRepository.findByUuidAndIsDeletedFalse(uuid).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                                String.format("User with UUID = %s is not found", uuid)));

        UserDto userDto = userMapper.mapUserToUserDto(user);

        return BaseApi.builder()
                .code(999)
                .message("You have loaded user successfully")
                .payload(userDto)
                .timestamp(LocalDateTime.now())
                .build();
    }

    @Override
    public BasePagingApi<?> loadUsers(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<User> userPage = userRepository.findAll(pageable);

        List<User> users = userPage.getContent();
        //users.forEach(user -> System.out.println("Test: " + user.getUserRoles().get(0).getRole().getName()));

        List<UserDto> userDtoList = userMapper.mapUsersToUserDtoList(users);

        return BasePagingApi.builder()
                .code(999)
                .message("You have loaded users successfully")
                .payload(userDtoList)
                .paging(BasePaging.builder()
                        .totalElements(userPage.getTotalElements())
                        .totalPages(userPage.getTotalPages())
                        .number(userPage.getNumber())
                        .numberOfElements(userPage.getNumberOfElements())
                        .size(userPage.getSize())
                        .build())
                .timestamp(LocalDateTime.now())
                .build();
    }

    @Transactional
    @Override
    public void createNew(CreateUserDto createUserDto) {

        if (!createUserDto.password().equals(createUserDto.confirmedPassword())) {
            return;
        }

        User user = userMapper.mapCreateUserDtoToUser(createUserDto);
        user.setUuid(UUID.randomUUID().toString());
        user.setIsDeleted(false);
        user.setIsVerified(false);

        // Transaction
        userRepository.save(user);
        System.out.println(user.getId());

        List<UserRole> userRoles = createUserDto.roleIds().stream()
                .map(roleId -> UserRole.builder()
                        .user(user)
                        .role(Role.builder().id(roleId).build())
                        .build())
                .toList();

        // Transaction
        userRoleRepository.saveAll(userRoles);

    }
}
