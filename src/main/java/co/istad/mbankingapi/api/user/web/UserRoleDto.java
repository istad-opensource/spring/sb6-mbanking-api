package co.istad.mbankingapi.api.user.web;

import co.istad.mbankingapi.api.authority.web.RoleDto;

public record UserRoleDto(RoleDto role) {
}
