package co.istad.mbankingapi.api.user.web;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record UpdateUserInfoDto(@NotBlank(message = "Name is required!")
                                @Size(min = 5, max = 30)
                                String name,
                                @NotBlank(message = "Gender is required!")
                                String gender,
                                @NotNull(message = "Is student is required!")
                                Boolean isStudent,
                                String studentCardNo) {
}
