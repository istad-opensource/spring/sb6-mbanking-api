package co.istad.mbankingapi.api.user;

import co.istad.mbankingapi.api.user.web.CreateUserDto;
import co.istad.mbankingapi.api.user.web.UpdateUserInfoDto;
import co.istad.mbankingapi.api.user.web.UserDto;
import org.mapstruct.*;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void mapUpdateUserInfoDtoToUser(UpdateUserInfoDto updateUserInfoDto,
                                    @MappingTarget User user);

    List<UserDto> mapUsersToUserDtoList(List<User> users);

    UserDto mapUserToUserDto(User user);

    // 1. Target Type: User
    // 2. Source Type: CreateUserDto
    User mapCreateUserDtoToUser(CreateUserDto createUserDto);

}
