package co.istad.mbankingapi.api.user;

import co.istad.mbankingapi.api.account.UserAccount;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, nullable = false)
    private String uuid;
    @Column(nullable = false)
    private String name;
    private String gender;
    private String profile;
    @Column(unique = true, nullable = false)
    private String email;
    @Column(nullable = false)
    private String password;
    @Column(unique = true, nullable = false)
    private String phoneNumber;
    @Column(unique = true)
    private String studentCardNo;
    private String oneSignalId;
    private String verifiedCode;
    private Boolean isVerified;
    private Boolean isStudent;
    private Boolean isDeleted;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<UserRole> userRoles;

    @OneToMany(mappedBy = "user")
    private List<UserAccount> userAccounts;

}
