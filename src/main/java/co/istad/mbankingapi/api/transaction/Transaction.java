package co.istad.mbankingapi.api.transaction;

import co.istad.mbankingapi.api.account.Account;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, nullable = false)
    private String uuid;
    @ManyToOne
    private Account senderAct;
    @ManyToOne
    private Account receiverAct;
    private BigDecimal amount;
    private Boolean isPayment;
    private String studentCardNo;
    @Column(columnDefinition = "text")
    private String remark;
    private LocalDateTime transactionAt;
}
