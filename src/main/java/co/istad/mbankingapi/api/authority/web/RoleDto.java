package co.istad.mbankingapi.api.authority.web;

import java.util.List;

public record RoleDto(String name,
                      List<AuthorityDto> authorities) {
}
