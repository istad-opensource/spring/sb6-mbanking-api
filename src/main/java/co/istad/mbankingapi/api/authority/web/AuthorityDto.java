package co.istad.mbankingapi.api.authority.web;

public record AuthorityDto(String name) {
}
