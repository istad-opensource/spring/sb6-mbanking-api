package co.istad.mbankingapi.init;

import co.istad.mbankingapi.api.authority.Authority;
import co.istad.mbankingapi.api.authority.AuthorityRepository;
import co.istad.mbankingapi.api.authority.Role;
import co.istad.mbankingapi.api.authority.RoleRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DataInitialization {

    private final AuthorityRepository authorityRepository;
    private final RoleRepository roleRepository;

    @PostConstruct
    public void init() {

        Authority readUser = Authority.builder().name("user:read").build();
        Authority writeUser = Authority.builder().name("user:write").build();
        Authority updateUser = Authority.builder().name("user:update").build();
        Authority deleteUser = Authority.builder().name("user:delete").build();

        Authority readAccount = Authority.builder().name("account:read").build();
        Authority writeAccount = Authority.builder().name("account:write").build();
        Authority updateAccount = Authority.builder().name("account:update").build();
        Authority deleteAccount = Authority.builder().name("account:delete").build();

        Authority readTransaction = Authority.builder().name("transaction:read").build();
        Authority writeTransaction = Authority.builder().name("transaction:write").build();
        Authority updateTransaction = Authority.builder().name("transaction:update").build();
        Authority deleteTransaction = Authority.builder().name("transaction:delete").build();

        authorityRepository.saveAll(List.of(
                readUser, writeUser, updateUser, deleteUser,
                readAccount, writeAccount, updateAccount, deleteAccount,
                readTransaction, writeTransaction, updateTransaction, deleteTransaction
        ));

        Role customer = Role.builder()
                .name("CUSTOMER")
                .authorities(List.of(
                        readUser, writeUser, updateUser,
                        readAccount, writeAccount, updateAccount,
                        readTransaction, writeTransaction
                ))
                .build();
        Role manager = Role.builder()
                .name("MANAGER")
                .authorities(List.of(
                        readUser, deleteUser,
                        readAccount, deleteAccount,
                        readTransaction
                ))
                .build();
        Role admin = Role.builder()
                .name("ADMIN")
                .authorities(List.of(
                        readUser, writeUser, updateUser, deleteUser,
                        readAccount, writeAccount, updateAccount, deleteAccount,
                        readTransaction, writeTransaction, updateTransaction, deleteTransaction
                ))
                .build();

        roleRepository.saveAll(List.of(customer, manager, admin));

    }

}
