package co.istad.mbankingapi.base;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record BasePagingApi <T> (String message,
                            Integer code,
                            T payload,
                            BasePaging paging,
                            LocalDateTime timestamp) {
}
