package co.istad.mbankingapi.base;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record BaseApi <T> (String message,
                           Integer code,
                           T payload,
                           LocalDateTime timestamp) {
}
