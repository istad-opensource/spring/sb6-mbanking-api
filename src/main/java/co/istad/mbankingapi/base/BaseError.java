package co.istad.mbankingapi.base;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record BaseError <T> (String message,
                        Integer code,
                        T errors,
                        LocalDateTime timestamp) {
}
