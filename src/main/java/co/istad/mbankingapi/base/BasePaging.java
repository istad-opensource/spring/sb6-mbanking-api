package co.istad.mbankingapi.base;

import lombok.Builder;

@Builder
public record BasePaging(long totalElements,
                         long totalPages,
                         long number,
                         long numberOfElements,
                         long size) {
}
