package co.istad.mbankingapi.base;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class BaseSpecification<T> {

    @Getter
    @Setter
    @AllArgsConstructor
    @Builder
    public static class SpecificationDto {

        private String column;
        private String value;
        private Operation operation;
        private String joinTable;

        public enum Operation{
            EQUAL, LIKE, IN, GREATER_THAN, LESS_THAN, BETWEEN, JOIN;
        }
    }

    public static class FilterDto {
        private List<SpecificationDto> searchRequestDto;

        private GlobalOperator globalOperator;

        public enum GlobalOperator{
            AND, OR;
        }
    }

    public Specification<T> getSearchSpecification(SpecificationDto searchRequestDto) {
        return new Specification<T>() {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get(searchRequestDto.getColumn()), searchRequestDto.getValue());
            }
        };
    }

    public Specification<T> getSearchSpecification(List<SpecificationDto> specsDtoList, FilterDto.GlobalOperator globalOperator) {

        return (root, query, criteriaBuilder) -> {

            List<Predicate> predicates = new ArrayList<>();

            for(SpecificationDto specsDto : specsDtoList){

                switch (specsDto.getOperation()) {
                    case EQUAL -> {
                        Predicate equal = criteriaBuilder.equal(root.get(specsDto.getColumn()), specsDto.getValue());
                        predicates.add(equal);
                    }
                    case LIKE -> {
                        Predicate like = criteriaBuilder.like(root.get(specsDto.getColumn()), "%" + specsDto.getValue() + "%");
                        predicates.add(like);
                    }
                    case IN -> {
                        String[] split = specsDto.getValue().split(",");
                        Predicate in = root.get(specsDto.getColumn()).in(Arrays.asList(split));
                        predicates.add(in);
                    }
                    case GREATER_THAN -> {
                        Predicate greaterThan = criteriaBuilder.greaterThan(root.get(specsDto.getColumn()), specsDto.getValue());
                        predicates.add(greaterThan);
                    }
                    case LESS_THAN -> {
                        Predicate lessThan = criteriaBuilder.lessThan(root.get(specsDto.getColumn()), specsDto.getValue());
                        predicates.add(lessThan);
                    }
                    case BETWEEN -> {
                        //"10, 20"
                        String[] split1 = specsDto.getValue().split(",");
                        Predicate between = criteriaBuilder.between(root.get(specsDto.getColumn()), Long.parseLong(split1[0]), Long.parseLong(split1[1]));
                        predicates.add(between);
                    }
                    case JOIN -> {
                        Predicate join = criteriaBuilder.equal(root.join(specsDto.getJoinTable()).get(specsDto.getColumn()), specsDto.getValue());
                        predicates.add(join);
                    }
                    default -> throw new IllegalStateException("Unexpected value: ");
                }

            }

            if(globalOperator.equals(FilterDto.GlobalOperator.AND)) {
                return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
            }else{
                return criteriaBuilder.or(predicates.toArray(new Predicate[0]));
            }
        };
    }

}
