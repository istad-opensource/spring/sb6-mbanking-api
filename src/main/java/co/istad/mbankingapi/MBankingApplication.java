package co.istad.mbankingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MBankingApplication {

    public static void main(String[] args) {
        SpringApplication.run(MBankingApplication.class, args);
    }

}
